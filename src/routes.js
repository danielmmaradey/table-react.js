//Dependencies
import React from 'react';
import { Route, Switch } from 'react-router-dom';

//Components
import App from './App';
import Dashboard from './components/pages/Dashboard';
import Users from './components/pages/Users';
import Products from './components/pages/Products';
import Configuration from './components/pages/Configuration';
import Help from './components/pages/Help';
import Page404 from './components/pages/Page404';

const AppRoutes = () =>
        <App>
            <Switch>
                <Route exact path="/" component={Dashboard}/>
                <Route exact path="/users" component={Users} />
                <Route exact path="/products" component={Products} />
                <Route exact path="/config" component={Configuration} />
                <Route exact path="/help" component={Help} />
                <Route component={Page404} />
            </Switch>
        </App>

export default AppRoutes;
  

