//DEPENDENCIES
import React, { Component } from 'react';

//SCSS
import './Table.scss';

class Table extends Component {
    state = {
        items: this.props.data
    }
    render() {
        const { items } = this.state;
        return (
            <table class="Table">
                <thead>
                    <tr>
                        <th scope="col">Number</th>
                        <th scope="col">Client</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Date</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        items && items.map(
                            (item, key) => <tr key={key} className="highlight" >
                                <td>{item.number}</td>
                                <td className="col-2"><span>{item.cliente.substring(0, 1)}</span>{item.cliente}</td>
                                <td>{item.amount}</td>
                                <td>{item.date}</td>
                                <td><i class={`fa fa-circle ${item.status === 1? "green" : "red"}`} aria-hidden="true"></i></td>
                            </tr>
                        )
                    }
                </tbody>
            </table>
        );
    }
}

export default Table;