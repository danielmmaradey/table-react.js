//DEPENDENCIES
import React, { Component } from 'react';
import axios from 'axios';

//SCSS
import './Header.scss';

class Header extends Component{
    state = {
        user: [],
        image: ""
    }
    
    componentDidMount = () =>{
        this.handleGetData();
    }

    handleGetData = () =>{
        const url = 'https://randomuser.me/api/?results=1';
        axios.get(url)
        .then(res => {this.setState({user: res.data.results})})
        .then(res => this.handleLogout())
    }

    handleLogout = () =>{
        console.log(this.state.user[0].picture.medium);
        this.setState({
            image: this.state.user[0].picture.medium
        })
    }
    render(){
        const { image } = this.state;
        return(
            <div className="Header">
                <i class="fa fa-search" aria-hidden="true"></i>
                <i class="fa fa-calendar" aria-hidden="true"></i>
                <i class="fa fa-bell" aria-hidden="true"></i>
                <img src={image} alt=""/>
            </div>
        );
    }
}

export default Header;