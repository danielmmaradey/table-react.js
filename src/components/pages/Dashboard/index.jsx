//DEPENDENCIES
import React, { Component } from 'react';

//COMPONENTS
import data from "../../../util/Data.jsx";
import Tabla from '../../Table/Table';

//SCSS
import './Dashboard.scss';

class Dashboard extends Component {

    state = {
        data: data
    }

    render() {
        const { data } = this.state
        const item = data.length;
        return (
            <div className="dashboard">
                <div className="content-dashboard">
                    <div className="sub-menu paddingt">
                        <h1>Lorem</h1>
                        <div className="content">
                            <button className="active"><h2>Invoices</h2></button>
                            <button><h2>Estimates</h2></button>
                            <button><h2>Recurring Invoices</h2></button>
                            <button><h2>Clients</h2></button>
                        </div>
                    </div>
                    <div className="option-button-tables">
                        <div className="content-1">
                            <i class="fa fa-print" aria-hidden="true"></i>
                            <i class="fa fa-file" aria-hidden="true"></i>
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </div>
                        <div className="content-2">
                            <h1>{item} items</h1>
                        </div>
                        <div className="content-3">
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                            <select name="" id="">
                                <option value="Default View">Default View</option>
                                <option value="option1">option 1</option>
                                <option value="option2">option 2</option>
                            </select>
                        </div>
                        <div className="content-4">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div className="Table">
                        <Tabla data={data}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Dashboard;