//Dependencies
import React, { Component } from 'react';
import propTypes from 'prop-types';
import { Link } from 'react-router-dom';

//Components
import Logo from './Logo';

//SCSS
import './Sidebar.scss';

class HeaderLeft extends Component {

    static propTypes = {
        items: propTypes.array.isRequired,
        path: propTypes.string.isRequired
    };

    render() {
        const { items, path } = this.props;
        return (
            <div className="sidebar">
                <div className="logo">
                    <Logo />
                </div>
                <div className="sidebar-menu">
                    <ul className="Menu">
                        {
                            items && items.map(
                                (item, key) => <li className={`side-menu  ${item.url === path ? "active" : ""} `} key={key} >
                                    <Link className="link" to={item.url}>
                                        <i className={item.icon}></i>
                                        {item.name}
                                    </Link>
                                </li>
                            )
                        }
                    </ul>
                </div>
            </div>
        );
    }
}

export default HeaderLeft;