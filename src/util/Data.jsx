export default
[
    {
        number: "h201",
        cliente: "Canada",
        amount: "$10.350",
        date: "May 1, 2019",
        status: 1
    },
    {
        number: "h501",
        cliente: "Venezuela",
        amount: "$5.350",
        date: "Jun 1, 2019",
        status: 0
    },
    {
        number: "h301",
        cliente: "Mexico",
        amount: "$30.610",
        date: "Feb 1, 2019",
        status: 1
    },
    {
        number: "h025",
        cliente: "Chile",
        amount: "$90.014",
        date: "Jan 1, 2019",
        status: 1
    }
]