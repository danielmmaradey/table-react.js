//Dependencies
import React, { Component, Fragment } from 'react';
import propTypes from 'prop-types';

//Components
import './index.css';
import items from './util/Nav';
import HeaderLeft from "./components/Sidebar";
import Header from './components/Header';

//Containers
import DefaultLayout from './containers/DefaultLayout';

class App extends Component {

  static propTypes = {
    children: propTypes.object.isRequired
  };


  render() {
    const { children } = this.props;
    const path = window.location.pathname;
    return (
      <Fragment>
        <div className="body-page">
          <div className="content-1">
            <HeaderLeft items={items} path={path} />
          </div>

          <div className="content-2">
            <Header />
            <DefaultLayout body={children} />
          </div>
        </div>
      </Fragment>

    );
  }
}

export default App;
