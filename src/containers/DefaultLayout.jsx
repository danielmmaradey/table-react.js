//DEPENDENCIES
import React, { Component } from 'react';
import propTypes from 'prop-types';

//SCSS
import './DefaultLayout.scss';

class DefaultLayout extends Component{
    static propTypes={
        body: propTypes.object.isRequired,
    };
    render(){
        const { body } = this.props;
        return(
            <div className="boddy-default">
                {body}
            </div>
        );
    }
}

export default DefaultLayout;